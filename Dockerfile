## ARGUMENTOS:
##   GANCIO_VERSION  tag/rama desde la que se construye gancio (defecto: master)
FROM registry.sindominio.net/debian as builder

RUN apt-get update && \
    apt-get install -y --no-install-recommends curl gnupg2 ca-certificates git jq wget

WORKDIR /

ARG GANCIO_VERSION=master
ARG GITLAB_PROJECT=48668

ADD les.gpg .
RUN gpg --import les.gpg && \
	echo "FB6752286CE8C5709A563A4E352918250B012177:6:" | gpg --import-ownertrust

# Install Node form Node Repo
ADD nodesource.gpg.key .
RUN cat nodesource.gpg.key | gpg --dearmor | tee /usr/share/keyrings/nodesource.gpg  >/dev/null
RUN echo 'deb [signed-by=/usr/share/keyrings/nodesource.gpg] https://deb.nodesource.com/node_20.x bookworm main' > /etc/apt/sources.list.d/nodesource.list
RUN echo 'deb-src [signed-by=/usr/share/keyrings/nodesource.gpg] https://deb.nodesource.com/node_20.x bookworm main' >> /etc/apt/sources.list.d/nodesource.list

## OLD: Yarn Classic
#ADD yarn.pubkey.gpg .
#RUN cat yarn.pubkey.gpg | apt-key add -
#RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
#RUN apt update && apt -y install yarn
#
# Install latest Gancio release

RUN git clone https://framagit.org/les/gancio.git

WORKDIR /gancio

RUN TAG_NAME=`curl -s https://framagit.org/api/v4/projects/48668/releases |jq -r '.[0].tag_name'` && \
        git verify-tag $TAG_NAME && \
        git checkout $TAG_NAME

ADD fadelkon.gpg .
RUN gpg --import fadelkon.gpg && \
	echo "86ABC201623DB6FC59C77357CA0DDF1D560013CD:6:" | gpg --import-ownertrust

WORKDIR /gancio/gancio_plugins

RUN git clone https://framagit.org/bcn.convocala/gancio-plugin-telegram-bridge.git

WORKDIR /gancio/gancio_plugins/gancio-plugin-telegram-bridge

RUN COMMIT=`git rev-parse --verify HEAD` && git verify-commit $COMMIT

RUN apt-get install -y --no-install-recommends npm nodejs
RUN npm install -g corepack
RUN npm i

WORKDIR /gancio

RUN yarn install && \
    yarn build && \
    yarn pack --filename=gancio.tgz

RUN tar xzf gancio.tgz && \
    mv package /tmp/gancio

FROM registry.sindominio.net/debian

ENV TZ=Europe/Madrid 

RUN apt-get update && apt-get install --no-install-recommends -y curl gpg git

# Old: Classic Yarn
#ADD yarn.pubkey.gpg .
#RUN cat yarn.pubkey.gpg | apt-key add -
##RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
#RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
#RUN apt update && apt -y install yarn

COPY --from=builder /tmp/gancio ./gancio

WORKDIR /gancio

# Install Node form Node Repo
ADD nodesource.gpg.key .
RUN cat nodesource.gpg.key | gpg --dearmor | tee /usr/share/keyrings/nodesource.gpg  >/dev/null
RUN echo 'deb [signed-by=/usr/share/keyrings/nodesource.gpg] https://deb.nodesource.com/node_20.x bookworm main' > /etc/apt/sources.list.d/nodesource.list
RUN echo 'deb-src [signed-by=/usr/share/keyrings/nodesource.gpg] https://deb.nodesource.com/node_20.x bookworm main' >> /etc/apt/sources.list.d/nodesource.list

RUN apt-get install -y --no-install-recommends npm nodejs
RUN npm install -g corepack

RUN yarn install --production && \    
	ln -s /gancio/server/cli.js /usr/bin/gancio

ADD entrypoint.sh /

RUN chmod 755 /entrypoint.sh

RUN mkdir /data

## Installer requirements
RUN mkdir /gancio/logs /gancio/uploads
RUN chmod 777 /gancio/logs /gancio/uploads

VOLUME ["/data"]

ENTRYPOINT [ "/bin/sh", "/entrypoint.sh" ]

CMD ["start", "--config", "/data/config.json"]
