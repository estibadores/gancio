#!/bin/bash

mkdir -p /data/logs
mkdir -p /data/uploads

## Change default logs  Path
[ -s /data/config.json ] && sed -i "s:/gancio/uploads:/data/uploads:" /data/config.json
## Change default logs path
[ -s /data/config.json ] && sed -i "s:/gancio/logs:/data/logs:" /data/config.json

exec /usr/bin/gancio $@
